//
//  Created by Nicola della Volpe on 12/12/2019.
//  Copyright © 2019 Nicola della Volpe. All rights reserved.
//

import UIKit
import RealityKit
import ARKit

class ViewController: UIViewController {
    
    @IBOutlet var arView: ARView!
    
    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var labelView: UILabel!
    
    @IBOutlet var buttonView: UIButton!
    
    @IBOutlet var backgroundView: UIView!
    
    
    @IBOutlet var titleLabel: UILabel!
    
    ///Uncomment to hide the status bar
//    override var prefersStatusBarHidden: Bool {
//              return true
//          }
    
    var sceneAnchor: Rocket.Scene!
    
    @IBAction func buttonPressed(_ sender: Any) {
        
        imageView.isHidden = true
        labelView.isHidden = true
        buttonView.isHidden = true
        backgroundView.isHidden = true
        titleLabel.isHidden = true
        sceneAnchor.notifications.prepared.post()

      
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let arConfiguration = ARWorldTrackingConfiguration()
//        arConfiguration.planeDetection = .horizontal
//        arView.session.run(arConfiguration)

        // Load the "Box" scene from the "Experience" Reality File
        sceneAnchor = try! Rocket.loadScene()
        
        // Add the box anchor to the scene
        arView.scene.anchors.append(sceneAnchor)
        
        setImage(imageName: "tesla")
        
        setbackground()
        
        buttonView.layer.cornerRadius = 8 
        
    }
    
    func setImage(imageName img: String) {
        
        imageView.contentMode = . scaleAspectFill
        if let newImage = UIImage(named: img) {
            imageView.image = newImage
        }
//        self.imageView.layer.cornerRadius = 8
        self.view.addSubview(imageView)
    }
    
  
    
    func setbackground(){
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = backgroundView.bounds
        
        //backgroundView = blurEffectView
        backgroundView.addSubview(blurEffectView)
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        ///Remove comment in order to activate people occlusion
        //       self.PeopleOcclusion()
        
    }
    
    fileprivate func PeopleOcclusion() {
        guard let config = arView.session.configuration as? ARWorldTrackingConfiguration else {
            fatalError("Unexpectedly failed to get the configuration.")
        }
        guard ARWorldTrackingConfiguration.supportsFrameSemantics(.personSegmentationWithDepth) else {
            fatalError("People occlusion is not supported on this device.")
        }
        
        config.frameSemantics.insert(.personSegmentationWithDepth)
        
        arView.session.run(config)
    }
    
    
}
